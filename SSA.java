import java.util.*;

public class SSA {
  String op; // some ops don't match
  Integer x; // in data
  Integer y; // in data
  Integer pos; // position in the SSA Vector
  List<Integer> xs; // only for Phi.
  Integer idomed; // idominater i. i idom self
  List<Integer> idoms; // for d in idoms, self idom d

  public SSA( Integer _pos, String _op, Integer _x, Integer _y) {
    pos = _pos; op = _op; x = _x; y = _y;
    idoms = new Vector<>();
  }

  public SSA( Integer _pos, String _phi, List<Integer> _xs) {
    xs = new Vector<>();
    idoms = new Vector<>();
    pos = _pos; op = _phi;
    xs.addAll(_xs);
  }
}