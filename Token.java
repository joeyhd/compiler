import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Token {
  static final int errorToken = 0;
  static final int timesToken = 1;
  static final int divToken = 2;
  static final int plusToken = 11;
  static final int minusToken = 12;
  static final int eqlToken = 20;
  static final int neqToken = 21;
  static final int lssToken = 22;
  static final int geqToken = 23;
  static final int leqToken = 24;
  static final int gtrToken = 25;
  static final int periodToken = 30;
  static final int commaToken = 31;
  static final int openbracketToken = 32;
  static final int closebracketToken = 34;
  static final int closeparenToken = 35;
  static final int becomesToken = 40;
  static final int thenToken = 41;
  static final int doToken = 42;
  static final int openparenToken = 50;
  static final int numberToken = 60;
  static final int identToken = 61;
  static final int semiToken = 70;
  static final int endToken = 80;
  static final int odToken = 81;
  static final int fiToken = 82;
  static final int elseToken = 90;
  static final int letToken = 100;
  static final int callToken = 101;
  static final int ifToken = 102;
  static final int whileToken = 103;
  static final int returnToken = 104;
  static final int varToken = 110;
  static final int arrToken = 111;
  static final int funcToken = 112;
  static final int procToken = 113;
  static final int beginToken = 150;
  static final int mainToken = 200;
  static final int eofToken = 255;

  public static String symToStr(int sym) {
    return tokenList[sym];
  }

  public static String tokenToStr(int token) {
    return symToStr(token);
  }

  public static final Map<String, Integer> tokenMap;
  static {
    tokenMap = new HashMap<String, Integer>();
    tokenMap.put("*", Token.timesToken);
    tokenMap.put("/", Token.divToken);
    tokenMap.put("+", Token.plusToken);
    tokenMap.put("-", Token.minusToken);
    tokenMap.put("==", Token.eqlToken);
    tokenMap.put("!=", Token.neqToken);
    tokenMap.put("<", Token.lssToken);
    tokenMap.put(">=", Token.geqToken);
    tokenMap.put("<=", Token.leqToken);
    tokenMap.put(">", Token.gtrToken);
    tokenMap.put(".", Token.periodToken);
    tokenMap.put(",", Token.commaToken);
    tokenMap.put("[", Token.openbracketToken);
    tokenMap.put("]", Token.closebracketToken);
    tokenMap.put(")", Token.closeparenToken);
    tokenMap.put("<-", Token.becomesToken);
    tokenMap.put("then", Token.thenToken);
    tokenMap.put("do", Token.doToken);
    tokenMap.put("(", Token.openparenToken);
    tokenMap.put(";", Token.semiToken);
    tokenMap.put("}", Token.endToken);
    tokenMap.put("od", Token.odToken);
    tokenMap.put("fi", Token.fiToken);
    tokenMap.put("else", Token.elseToken);
    tokenMap.put("let", Token.letToken);
    tokenMap.put("call", Token.callToken);
    tokenMap.put("if", Token.ifToken);
    tokenMap.put("while", Token.whileToken);
    tokenMap.put("return", Token.returnToken);
    tokenMap.put("var", Token.varToken);
    tokenMap.put("array", Token.arrToken);
    tokenMap.put("function", Token.funcToken);
    tokenMap.put("procedure", Token.procToken);
    tokenMap.put("{", Token.beginToken);
    tokenMap.put("main", Token.mainToken);
  }

  private static final String[] tokenList = new String[256];
  static {
    tokenList[1] = "*";
    tokenList[2] = "/";
    tokenList[11] = "+";
    tokenList[12] = "-";
    tokenList[20] = "==";
    tokenList[21] = "!=";
    tokenList[22] = "<";
    tokenList[23] = ">=";
    tokenList[24] = "<=";
    tokenList[25] = ">";
    tokenList[30] = ".";
    tokenList[31] = ",";
    tokenList[32] = "[";
    tokenList[34] = "]";
    tokenList[35] = ")";
    tokenList[40] = "<-";
    tokenList[41] = "then";
    tokenList[42] = "do";
    tokenList[50] = "(";
    tokenList[60] = "num";
    tokenList[61] = "id";
    tokenList[70] = ";";
    tokenList[80] = "}";
    tokenList[81] = "od";
    tokenList[82] = "fi";
    tokenList[90] = "else";
    tokenList[100] = "let";
    tokenList[101] = "call";
    tokenList[102] = "if";
    tokenList[103] = "while";
    tokenList[104] = "return";
    tokenList[110] = "var";
    tokenList[111] = "array";
    tokenList[112] = "function";
    tokenList[113] = "procedure";
    tokenList[150] = "{";
    tokenList[200] = "main";
  }
}
