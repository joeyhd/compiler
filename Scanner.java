import java.util.*;

// !!!!! Use Reg Exp to improve performance.

public class Scanner {
  public int GetSym() {
    Next(); return sym;
  }
  public int number; // value of the last number encountered
  public int id; // id of the last identifier encountered

  public void Error(String errorMst) { // signal an error message (calls FileReader.Error in turn)
    sym = Token.errorToken;
    fr.Error(errorMst);
  }

  public Scanner(String fileName){ // constructor: open file and scan the first token into 'sym'
    fr = new FileReader(fileName);
    idtbl = new Vector<>();
    idtbl.add("InputNum");
    idtbl.add("OutputNum");
    peek = fr.GetSym();
  }

  /* identifier table methods */
  public String Id2String(int id) {
    return idtbl.elementAt(id);
  }
  public int String2Id(String name) {
    for (int id = 0; id < idtbl.size(); ++id) {
      if(idtbl.elementAt(id).equals(name)) {
        return id;
      }
    }
    // insert if not exist.
    idtbl.add(name);
    return idtbl.size()-1;
  }

  private FileReader fr;
  private Vector<String> idtbl;
  private char peek; // sym of peek
  private int sym; // the current token on the input, 0=error token, 255=end-of-file token

  private void Next() { // advance to the next token
    String token = "";
    int state = -1;
    while(peek != (char)0x00 && peek != (char)0xff) {
      switch (state) {
        case -1 :
        {
          token += peek;
          if (isDigit(peek)) {
            number = peek - '0';
            state = 28;
          } else if (isLetter_(peek)) {
            state = 26;
          } else{
            state = firstState[peek];
          }
          peek = fr.GetSym();
          break;
        }
        case 0:
          sym = 0;
          Error("Lexical Error");
          return;
        case 5:
          token += peek;
          if(peek == '=') {
            state = 6;
          } else {
            state = 0;
          }
          peek = fr.GetSym();
          break;
        case 7:
          token = "";
          state = -1;
          break;
        case 8:
          token += peek;
          if(peek == '=') {
            state = 9;
          } else {
            state = 0;
          }
          peek = fr.GetSym();
          break;
        case 10:
          if(peek == '-') {
            token += peek;
            state = 12;
            peek = fr.GetSym();
          } else if(peek == '=') {
            token += peek;
            state = 13;
            peek = fr.GetSym();
          } else {
            state = 11;
          }
          break;
        case 14:
          if(peek == '=') {
            token += peek;
            state = 15;
            peek = fr.GetSym();
          } else {
            state = 16;
          }
          break;
        case 26:
          if(!(isLetter_(peek) || isDigit(peek))) {
            state = 27;
          } else {
            token += peek;
            peek = fr.GetSym();
          }
          break;
        case 27:
          if(Token.tokenMap.containsKey(token)) {
            sym = Token.tokenMap.get(token);
          } else {
            sym = Token.identToken;
            id = String2Id(token);
          }
          return;
        case 28:
          if(!isDigit(peek)) {
            if(isLetter_(peek)) {
              state = 0;
            } else {
              state = 29;
            }
          } else {
            number = number * 10 + peek - '0';
            peek = fr.GetSym();
          }
          break;
        case 29:
          sym = Token.numberToken;
          return;
        default:
          sym = Token.tokenMap.get(token);
          return;
      }
    }
    if(peek == 0xff && !token.equals("") && !token.equals(" ") && !token.equals("\n") && !token.equals("\t")) {
      if(Token.tokenMap.containsKey(token)) {
        sym = Token.tokenMap.get(token);
      } else {
        sym = 0x00;
      }
    } else {
      sym = (int) peek; // 0x00 -> errorToken, 0xff -> eofToken
    }
    return;
  }

  // simple judges
  private boolean isDigit(char ch) {
    return ch >= 48 && ch <= 57;
  }
  private boolean isLetter_(char ch) {
    return (ch >= 65 && ch <= 90) || // A~Z
        (ch >= 97 && ch <= 122) || // a~z
        ch == '_';
  }


  private static final int firstState[] = new int[256];
  static {
    firstState['*'] = 1;
    firstState['/'] = 2;
    firstState['+'] = 3;
    firstState['-'] = 4;
    firstState['='] = 5;
    firstState['!'] = 8;
    firstState['<'] = 10;
    firstState['>'] = 14;
    firstState['.'] = 17;
    firstState[','] = 18;
    firstState['['] = 19;
    firstState[']'] = 20;
    firstState[')'] = 21;
    firstState['('] = 22;
    firstState[';'] = 23;
    firstState['}'] = 24;
    firstState['{'] = 25;
    firstState['\n'] = 7;
    firstState['\t'] = 7;
    firstState[' '] = 7;
  } // The state after first input char

}
