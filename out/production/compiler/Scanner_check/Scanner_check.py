import commands

def diff(ifile, ofile):
  i = open(ifile);
  o = open(ofile);
  ic = " "
  oc = " "
  cnt = 0
  mes = ""
  while True:
    while ic == " " or ic == "\n" or ic == "\t" or ic == "\r":
      ic = i.read(1)
      cnt += 1
    while oc == " " or oc == "\n" or oc == "\t" or oc == "\r":
      oc = o.read(1)
    mes += ic
    if (oc and not ic) or (ic and not oc) :
      return -1
    if not (oc or ic):
      return 1
    if ic != oc:
      return -1
    ic = " "
    oc = " "

lists = ["big.txt", "cell.txt", "factorial.txt"]
for i in range(9):
  lists.append("test00"+str(i+1)+".txt");
  lists.append("test01"+str(i+1)+".txt");
  lists.append("test02"+str(i+1)+".txt");
lists.append("test010.txt");
lists.append("test020.txt");
lists.append("test030.txt");
lists.append("test031.txt");

for item in lists:
  ifile = "Scanner_check/"+item;
  ofile = "Scanner_check/"+item.split('.')[0]+"_o."+item.split('.')[1];
  print "compare "+ifile
  commands.getoutput("java compile "+ifile+" > "+ofile)
  if(diff(ifile, ofile) == -1):
    break
