import json

N0 = "NT_Program"
N1 = "NT_Global_claims"
N2 = "NT_Fun_Proc_defs"
# N3 = "NT_Procedure_defs_Unused"
N3 = "NT_optRvalue"
N4 = "NT_Main_block"
N5 = "NT_function_def"
N6 = "NT_procedure_def"
N7 = "NT_claims"
N8 = "NT_optFormalParams"
N9 = "NT_local_claims"
N10 = "NT_block"
N11 = "NT_returnStatement"
N12 = "NT_arr_size"
N13 = "NT_sentence"
N14 = "NT_assignment"
N15 = "NT__call"
N16 = "NT_branch"
N17 = "NT_whileLoop"
N18 = "NT_rvalue"
N19 = "NT_optActuralParams"
N20 = "NT_condition"
N21 = "NT_optElseBranch"
N22 = "NT_optContFormalParams"
N23 = "NT_optContActuralParams"
N24 = "NT_factor"
N25 = "NT_term"
N26 = "NT_expression"
N27 = "NT_optArr_size"
N28 = "NT_optContId"
N29 = "NT_optBlock"
N30 = "NT_compOperator"
N31 = "NT_optContTerm"
N32 = "NT_optContExpression"
N33 = "NT_optContBlock"
N34 = "NT_lvalue"
N35 = "NT_proc_optFormalParam"

# @N0
Program = {"main": ["main", N1, N2, "{", N10, "}", "."]}
# @N1
Global_claims = {"var": [N7], "array": [N7], "function": [], "procedure": [], "{": []}
# @N2
Function_defs = {"function": [N5, N2], "procedure": [N6, N2], "{": []}
# Procedure_defs = {"procedure": [N6, N3], "{": []}
# @N3
optRvalue = {"id": [N18], "num": [N18], ")": [], "(": [N18], "call": [N18]}
# @N4
Main_block = {"{": ["{", N10, "}", "."]}
# @N5
function_def = {"function": ["function", "id", "(", N8, ")", ";", N9, "{", N10, "}", ";"]}
# @N6
procedure_def = {"procedure": ["procedure", "id", N35, ";", N9, "{", N10, "}", ";"]}
# @N7
claims = {"var": ["var", "id", N28, ";", N7], "array": ["array", N12, "id", N28, ";", N7], "function": [], "procedure": [], "{": []}
# @N8
optFormalParams = {"id": ["id", N22], ")": []}
# @N9
local_claims = {"var": [N7], "array": [N7], "function": [], "procedure": [], "{": []}
# @N10
block = {"let": [N13, N29], "call": [N13, N29], "if": [N13, N29], "while": [N13, N29], "return": [N13, N29]}
# @N11
returnStatement = {"return": ["return", N18]}
# @N12
arr_size = {"[": ["[", N18, "]", N27]}
# @N13
sentence = {"let": [N14], "call": [N15], "if": [N16], "while": [N17], "return": [N11]}
# @N14
assignment = {"let": ["let", N34, "<-", N18]}
# @N15
call = {"call": ["call", "id", N19]}
# @N16
branch = {"if": ["if", N20, "then", N10, N21, "fi"]}
# @N17
whileLoop = {"while": ["while", N20, "do", N10, "od"]}
# @N18
rvalue = {"id": [N26], "num": [N26], "(": [N26], "call": [N26]}
# @N19
optActuralParams = {"(": ["(", N3, N23, ")"], ";": [], "}": [], "od": [], "fi": [], "else": []}
# @N20
condition = {"id": [N18, N30, N18], "num": [N18, N30, N18], "(": [N18, N30, N18], "call": [N18, N30, N18]}
# @N21
optElseBranch = {"fi": [], "else": ["else", N10], "return": []}
# @N22
optContFormalParams = {",": [",", "id", N22], ")": []}
# @N23
optContActuralParams = {",": [",", N18, N23], ")": []}
# @N24
factor = {"id": ["id", N27], "num": ["num"], "]": [], "(": ["(", N26, ")"], "call": [N15]}
# @N25
term = {"id": [N24, N31], "num": [N24, N31], "]": [], "(": [N24, N31], "call": [N24, N31]}
# @N26
expression = {"id": [N25, N32], "num": [N25, N32], "(": [N25, N32], "call": [N25, N32]}
# @N27
optArr_size = {"id": [], "[": ["[", N18, "]", N27], "*": [], "/": [], "+": [], "-": [], "==": [],
               "!=": [], "<": [], ">=": [], "<=": [], ">": [], ",": [], "]": [], ")": [], "<-": [], "then": [], "do": [],
               "}": [], ";": [], "od": [], "fi": [], "else": []}
# @N28
optId = {",": [",", "id", N28], ";": []}
# @N29
optBlock = {";": [";", N33], "}": [], "od": [], "fi": [], "else": []}
# @N30
compOperator = {"==": ["=="], "!=": ["!="], "<": ["<"], ">=": [">="], "<=": ["<="], ">": [">"]}
# @N31
optContTerm = {"*": ["*", N25], "/": ["/", N25], "+": [], "-": [], "==": [], "!=": [], "<": [], ">=": [], "<=": [],
               ">": [], ",": [], "]": [], ")": [], "then": [], "do": [], "}": [], ";": [], "od": [], "fi": [], "else": []}
# @N32
optContExpression = {"+": ["+", N26], "-": ["-", N26], "==": [], "!=": [], "<": [], ">=": [], "<=": [],
                     ">": [], ",": [], "]": [], ")": [], "then": [], "do": [], "}": [], ";": [], "od": [],
                     "fi": [], "else": []}
# @N33
optContBlock = {"let": [N10], "call": [N10], "if": [N10], "while": [N10], "return": [N10]}
# @N34
lvalue = {"id": ["id", N27]}
# @N35
proc_optFormalParam = {"(": ["(", N8, ")"], ";": []}

grammar = {N0: Program, N1: Global_claims, N2: Function_defs, N3: optRvalue, N4: Main_block,
           N5: function_def, N6: procedure_def, N7: claims, N8: optFormalParams,
           N9: local_claims, N10: block, N11: returnStatement, N12: arr_size,
           N13: sentence, N14: assignment, N15: call, N16: branch,
           N17: whileLoop, N18: rvalue, N19: optActuralParams, N20: condition,
           N21: optElseBranch, N22: optContFormalParams, N23: optContActuralParams, N24: factor,
           N25: term, N26: expression, N27: optArr_size, N28: optId,
           N29: optBlock, N30: compOperator, N31: optContTerm, N32: optContExpression,
           N33: optContBlock, N34: lvalue, N35: proc_optFormalParam}

ofile = open("grammar.json", 'w')
ofile.write(json.dumps(grammar))
ofile.close()
