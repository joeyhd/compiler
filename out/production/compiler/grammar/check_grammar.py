import json
import pprint as pp

ifile = open("grammar.json", 'r')
grammar = json.loads(ifile.read())
ifile.close()

s = ["NT_Program"]

token = ""
while s:
  TNT = s.pop()
  if(TNT[0:2] == "NT"):
    NT_grammar = grammar[TNT]
    while token == "":
      token = input()
      if token not in NT_grammar:
        print("type error or grammar error. retype or quit")
        token = ""
    if token not in NT_grammar:
      print("grammar error")
      exit()
    NT_list = NT_grammar[token].copy()
    NT_list.reverse()
    s += NT_list
  else:
    while token == "":
      token = input()
      if token != TNT:
        print("type error or grammar error. retype or quit")
        token = ""
    if token != TNT:
      print("grammar error")
      exit()
    token = ""

