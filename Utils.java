import java.io.IOException;
import java.nio.charset.*;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;
import java.text.DateFormat;
import java.util.Date;

public class Utils {
  public static List<String> readFileLines(String path) {
    List<String> lines = new Vector<>();
    try {
      Charset encoding = Charset.defaultCharset();
      lines = Files.readAllLines(Paths.get(path), encoding);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
    return lines;
  }

  public static void saveSSAs(Vector<SSA> ssalist, List<varnode> data) {
    DateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    Date date = new Date();
    Path file = Paths.get("log/"+dataFormat.format(date));
    String log = "";
    for (int i = 0; i < ssalist.size(); ++i) {
      String PC = Integer.toString(ssalist.get(i).pos);
      String op = ssalist.get(i).op;
      if(op.equals("phi")) {
        String str_var = "";
        for( Integer v : ssalist.get(i).xs) {
          str_var += getDataStr(v, data) + ",";
        }
        log += PC + "  " + op + "  " + str_var + "\n";
      } else {
        String str_x = getDataStr(ssalist.get(i).x, data);
        String str_y = getDataStr(ssalist.get(i).y, data);
        log += PC + "  " + op + "  " + str_x + "  " + str_y + "\n";
      }
    }
    try {
      Files.write(file, log.getBytes());
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  static private String getDataStr(Integer v, List<varnode> data) {
    String str = "";
    if(!v.equals(-1)) {
      varnode d = data.get(v);
      if(d.type == varnode.Const) {
        str += "Const@" + d.val.toString();
      } else if(d.type == varnode.Array){
        str += "Array@" + v.toString() + "";
      } else {
        str += "Var@(" + d.def.toString() + ")";
      }
    } else {
      str += "-1";
    }
    return str;
  }

  public static void checkAllGrammar() {
    for (int i = 0; i < testLists.length; i++) {
      System.out.println("============================");
      System.out.println("Testing " + testLists[i]);
      IRgenerator ck = new IRgenerator();
      Scanner fr = new Scanner(testLists[i]);
      boolean res = checkGrammar(ck, fr);
      System.out.println();
      if(!res) {
        break;
      }
    }
  }

  public static void generateIRall() {
    for (int i = 1; i < testLists.length; i++) {
      IRgenerator ir = new IRgenerator();
      System.out.println("============================");
      System.out.println("Generate " + testLists[i]);
      ir.generate(testLists[i]);
    }
  }

  public static boolean checkGrammar(IRgenerator ck, Scanner fr) {
    int sym;
    boolean state = true;
    while(state && (sym = fr.GetSym()) != 0xff) {
      printToken(fr, sym);
      state = ck.check(sym);
    }
    if (!state) {
      System.out.println(ck.getErrMsg());
    } else if (!ck.isFinish()) {
      state = false;
      System.out.println("Unfinished program.");
    } else {
      System.out.println("Grammar correct.");
    }
    return state;
  }

  // construct original program from scanned symbols.
  public static void construct(Scanner fr) {
    int sym;
    while((sym = fr.GetSym()) != 0xff) {
      // construct
      printToken(fr, sym);
    }
  }

  private static String[] testLists = new String[34];
  static {
    testLists[0] = "testcase/test001.txt";
    testLists[1] = "testcase/test002.txt";
    testLists[2] = "testcase/test003.txt";
    testLists[3] = "testcase/test004.txt";
    testLists[4] = "testcase/test005.txt";
    testLists[5] = "testcase/test006.txt";
    testLists[6] = "testcase/test007.txt";
    testLists[7] = "testcase/test008.txt";
    testLists[8] = "testcase/test009.txt";
    testLists[9] = "testcase/test010.txt";
    testLists[10] = "testcase/test011.txt";
    testLists[11] = "testcase/test012.txt";
    testLists[12] = "testcase/test013.txt";
    testLists[13] = "testcase/test014.txt";
    testLists[14] = "testcase/test015.txt";
    testLists[15] = "testcase/test016.txt";
    testLists[16] = "testcase/test017.txt";
    testLists[17] = "testcase/test018.txt";
    testLists[18] = "testcase/test019.txt";
    testLists[19] = "testcase/test020.txt";
    testLists[20] = "testcase/test021.txt";
    testLists[21] = "testcase/test022.txt";
    testLists[22] = "testcase/test023.txt";
    testLists[23] = "testcase/test024.txt";
    testLists[24] = "testcase/test025.txt";
    testLists[25] = "testcase/test026.txt";
    testLists[26] = "testcase/test027.txt";
    testLists[27] = "testcase/test028.txt";
    testLists[28] = "testcase/test029.txt";
    testLists[29] = "testcase/test030.txt";
    testLists[30] = "testcase/test031.txt";
    testLists[31] = "testcase/big.txt";
    testLists[32] = "testcase/cell.txt";
    testLists[33] = "testcase/factorial.txt";
  }


  private static void printToken(Scanner fr, int sym) {
    if(sym == 60) {
      System.out.print(fr.number);
    } else if(sym == 61) {
      System.out.print(fr.Id2String(fr.id));
    } else {
      System.out.print(decons[sym]);
    }
  }

  public static final String decons[] = new String[256];
  static {
    decons[1] = " * ";
    decons[2] = " / ";
    decons[11] = " + ";
    decons[12] = " - ";
    decons[20] = " == ";
    decons[21] = " != ";
    decons[22] = " < ";
    decons[23] = " >= ";
    decons[24] = " <= ";
    decons[25] = " > ";
    decons[30] = ".";
    decons[31] = ", ";
    decons[32] = "[";
    decons[34] = "] ";
    decons[35] = ")";
    decons[40] = " <- ";
    decons[41] = " then\n";
    decons[42] = " do\n";
    decons[50] = "(";
    decons[70] = ";\n";
    decons[80] = "\n}\n";
    decons[81] = "\nod";
    decons[82] = "\nfi";
    decons[90] = "else \n";
    decons[100] = "let ";
    decons[101] = "call ";
    decons[102] = "if ";
    decons[103] = "while ";
    decons[104] = "return ";
    decons[110] = "var ";
    decons[111] = "array ";
    decons[112] = "function ";
    decons[113] = "procedure ";
    decons[150] = "{\n";
    decons[200] = "main\n";
  }
}
