import com.google.gson.*;
import javafx.util.Pair;
import java.util.*;

public class IRgenerator {
  private String errMsg = "";
  private static final String grammarFile = "grammar/grammar.json";
  private static JsonObject grammar;
  private static Stack<String> tntStack = new Stack<>();

  public IRgenerator() {
    loadGrammar();
    tntStack.push("NT_Program");
  }

  public boolean isFinish() {
    return tntStack.empty();
  }

  public boolean check(int sym) {
    String tokenStr = Token.symToStr(sym);
    if(tokenStr == "return") {
      System.out.print("");
    }
    JsonObject NT_grammar;
    while (!tntStack.empty()) {
      String tnt = tntStack.pop();
      if (tnt.length() > 2 && tnt.substring(0, 2).equals("NT")) {
        NT_grammar = grammar.get(tnt).getAsJsonObject();
        if(NT_grammar.has(tokenStr)) {
          JsonArray pushItem = NT_grammar.get(tokenStr).getAsJsonArray();
          for (int i = pushItem.size()-1; i >= 0; i--) {
            tntStack.push(pushItem.get(i).getAsString());
          }
        } else {
          errMsg = "\ntoken " + tokenStr + " not in NT grammar " + tnt;
          return false;
        }
      } else {
        if (tnt.equals(tokenStr)) {
          break;
        } else {
          errMsg = "\ntoken " + tokenStr + " not same to T " + tnt;
          return false;
        }
      }
    }
    return true;
  }

  public String getErrMsg() {
    return errMsg;
  }

  private static void loadGrammar() {
    List<String> lines = Utils.readFileLines(grammarFile);
    grammar = new JsonParser().parse(lines.get(0)).getAsJsonObject();
  }

  /* ====================================== */
  // generate SSA

  // data_p
  List<varnode> globaldata = new Vector<>(); // all data. save constant, variable, expression(intermediate result),
  // pos
  List<Integer> globalvarpos = new Vector<>(); // save var position in the "data". different var can have same varpos. e.g. x <- 50. we will create a constant 50 in data, and varpos of x will be the same position of 50
  // id -> varinfo
  Map<Integer, varinfo> globalid; // given id, get the position of var pos // 一旦被生成，里面的数据就不会再变了(pos只会被改一次)。
  // cse_key -> data_p
  Map<String, Integer> globaldict = new HashMap<>(); // given a hash value, get the position in data.
  // key:   constant: str("const@")+str(value)
  // key:        var: str("var@")+str(position) (position in data)
  // key: expression: str(op)+str(position_x)+str(",")+str(position_y)
  Integer Base = 0; // should be update when generate instruction

  // function and procedure
  List<FuncProc> FPs = new Vector<>();
  Map<Integer, Integer> FPid = new HashMap<>(); // id -> FPs_id

  Scanner sr;
  int genSym = -1;
  static int gPC = 0; // SSA position

  public Vector<SSA> generate(String srcfile) {
    gPC = 0;
    sr = new Scanner(srcfile);
    Vector<SSA> res = genProgram();
    // Utils.saveSSAs(res, globaldata);
    return res;
  }

  // N0
  private Vector<SSA> genProgram() {
    final String part = "NT_program";
    Vector<SSA> program = new Vector<>();
    Vector<FuncProc> FPs = new Vector<>();
    globalid = new HashMap<>();
    FPs.add(InputNum()); FPs.add(OutputNum());
    // pre
    varnode global_base = new varnode(-1, varnode.Const);
    global_base.val = Base;
    globaldata.add(global_base);
    // "main"
    if(!nextSymEqualTo(Token.mainToken, part)) return null;
    // N1
    pack _d = new pack();
    _d.idmap = globalid; _d.varpos = globalvarpos;
    _d.PC = gPC; _d.data = globaldata; _d.dict = globaldict;
    genClaims(program, _d);
    // N2
    genFuncProcs(FPs, globalid);
    // "{"
    if(!nextSymEqualTo(Token.beginToken, part)) return null;
    // N10
    genBlock(program, _d);
    // "}"
    if(!nextSymEqualTo(Token.endToken, part)) return null;
    // "."
    if(!nextSymEqualTo(Token.periodToken, part)) return null;
    // success
    program.add(new SSA(_d.PC++, "end", -1, -1));
    return program;
  }

  // N1
  private Boolean genClaims(Vector<SSA> claims, pack _d) {
    final String part = "NT_Claims";
    Integer bias = Base;
    return genClaim(claims, bias, _d) != -1;
  }


  // N2
  private Boolean genFuncProcs(Vector<FuncProc> FPs, Map<Integer, varinfo> globalid) {
    final String part = "NT_FuncProcs";
    nextSym();
    if(genSym == Token.funcToken) {
      // N5
      FPs.add(genFunction());
      // N2
      genFuncProcs(FPs, globalid);
      return true;
    } else if (genSym == Token.procToken) {
      // N6
      FPs.add(genProcedure(globalid));
      // N2
      genFuncProcs(FPs, globalid);
      return true;
    } else if (genSym == Token.beginToken) {
      return true;
    } else {
      genError(part);
      return false;
    }
  }

  // N5 ** check return
  private FuncProc genFunction() {
    final String part = "NT_function_def";
    FuncProc func = new FuncProc();
    func.isfunction = true;
    pack _d = new pack();
    _d.varpos = func.varpos; _d.PC = 0;
    _d.idmap = func.localid; _d.data = func.data;
    _d.dict = func.dict;
    genLoadGlobalVar(func.ssa, _d);
    // "function"
    if(!nextSymEqualTo(Token.funcToken, part)) return null;
    // id
    if(!nextSymEqualTo(Token.identToken, part)) return null;
    func.funcid = sr.id;
    varnode node = new varnode(-1, varnode.Function);
    globaldata.add(node); globalvarpos.add(globaldata.size()-1);
    varinfo info = new varinfo();
    info.pos = globalvarpos.size()-1;
    globalid.put(sr.id, info);
    // "("
    if(!nextSymEqualTo(Token.openparenToken, part)) return null;
    // N8
    func.paramnum = genOptFormalParams(func.ssa, _d);
    // ")"
    if(!nextSymEqualTo(Token.closeparenToken, part)) return null;
    // ";"
    if(!nextSymEqualTo(Token.semiToken, part)) return null;
    // N9
    genClaims(func.ssa, _d);
    // "{"
    if(!nextSymEqualTo(Token.beginToken, part)) return null;
    // N10
    genBlock(func.ssa, _d);
    // "}"
    if(!nextSymEqualTo(Token.endToken, part)) return null;
    // ";"
    if(!nextSymEqualTo(Token.semiToken, part)) return null;
    return func;
  }

  private Boolean genLoadGlobalVar(Vector<SSA> loadglobalval, pack _d) {
    final String part = "loadglobalvar";
    for(Integer id: globalid.keySet()) {
      varinfo ginfo = globalid.get(id);
      varinfo info;
      if(ginfo.isarray) {
        info = new varinfo(ginfo.arrshape);
        info.pos = _d.varpos.size();
      } else {
        info = new varinfo();
        info.pos = _d.varpos.size();
      }
      _d.idmap.put(id, info);
      if(info.isarray) {
        _d.varpos.add(dataAddArray(id, _d));
      } else {
        _d.varpos.add(dataAddVar(id, _d));
        loadglobalval.add(new SSA(_d.PC++, "load", -1, dataGetConst(globalid.get(id).address, _d)));
      }
    }
    return true;
  }

  // N6
  private FuncProc genProcedure(Map<Integer, varinfo> globalid) {
    final String part = "NT_procedure_def";
    FuncProc proc = new FuncProc();
    proc.isfunction = false;
    pack _d = new pack();
    _d.idmap = proc.localid; _d.data = proc.data;
    _d.PC = 0; _d.varpos = proc.varpos;
    _d.dict = proc.dict;
    genLoadGlobalVar(proc.ssa, _d);
    // "function"
    if(!nextSymEqualTo(Token.procToken, part)) return null;
    // id
    if(!nextSymEqualTo(Token.identToken, part)) return null;
    proc.funcid = sr.id;
    varnode node = new varnode(-1, varnode.Procedure);
    globaldata.add(node); globalvarpos.add(globaldata.size()-1);
    varinfo info = new varinfo();
    info.pos = globalvarpos.size()-1;
    globalid.put(sr.id, info);
    // N35
    proc.paramnum = genProcOptFormalParams(proc.ssa, _d);
    // ";"
    if(!nextSymEqualTo(Token.semiToken, part)) return null;
    // N9
    genClaims(proc.ssa, _d);
    // "{"
    if(!nextSymEqualTo(Token.beginToken, part)) return null;
    // N10
    genBlock(proc.ssa, _d);
    // "}"
    if(!nextSymEqualTo(Token.endToken, part)) return null;
    // ";"
    if(!nextSymEqualTo(Token.semiToken, part)) return null;
    return proc;
  }

  // N7
  private Integer genClaim(Vector<SSA> claim, Integer bias, pack _d) {
    final String part = "NT_claim";
    nextSym();
    if(genSym == Token.varToken) {
      resetSym();
      // id
      if(!nextSymEqualTo(Token.identToken, part)) return -1;
      int id = sr.id;
      varinfo info = new varinfo();
      info.pos = _d.varpos.size(); info.address = bias; bias ++;
      _d.varpos.add(-1); _d.idmap.put(id, info); // for var, varpos default -1
      // N28
      List<Integer> ids = genOptContId();
      for (int i = 0; i < ids.size(); i++ ){
        info = new varinfo();
        info.pos = _d.varpos.size(); info.address = bias; bias ++;
        _d.varpos.add(-1); _d.idmap.put(ids.get(i), info);
      }
      // ";"
      if(!nextSymEqualTo(Token.semiToken, part)) return -1;
      // N7
      return genClaim(claim, bias, _d);
    } else if(genSym == Token.arrToken) {
      resetSym();
      // N12
      List<Integer> arrshapeid = new Vector<>(); // List of id in data.
      genArrSize(claim, arrshapeid, _d, false);
      // id
      if(!nextSymEqualTo(Token.identToken, part)) return -1;
      int id = sr.id;
      List<Integer>arrshape = new Vector<>();
      for(int k = 0; k < arrshapeid.size(); k++) {
        arrshape.add(_d.data.get(arrshapeid.get(k)).val);
      }
      varinfo info = new varinfo(arrshape);
      info.pos = _d.varpos.size(); info.address = bias; bias += info.size;
      _d.varpos.add(-2); _d.idmap.put(id, info); // for array, varpos default -2
      // N28
      List<Integer> ids = genOptContId();
      for (int i = 0; i < ids.size(); i++ ){
        info = new varinfo(arrshape);
        info.pos = _d.varpos.size(); info.address = bias; bias += info.size;
        _d.varpos.add(-1); _d.idmap.put(ids.get(i), info);
      }
      // ";"
      if(!nextSymEqualTo(Token.semiToken, part)) return -1;
      // N7
      return genClaim(claim, bias, _d);
    } else if(genSym == Token.funcToken || genSym == Token.beginToken || genSym == Token.procToken) {
      return bias;
    }
    genError(part);
    return -1;
  }

  // N8
  private Integer genOptFormalParams(Vector<SSA> optformalparams, pack _d) {
    final String part = "optformalparams";
    nextSym();
    if(genSym == Token.closeparenToken) {
      return 0;
    } else if(genSym == Token.identToken) {
      varinfo info = new varinfo();
      _d.data.add(new varnode(-2, varnode.FormalParam));
      info.pos = _d.data.size()-1;
      _d.idmap.put(sr.id, info);
      resetSym();
      // N22
      return genContFormalParams(optformalparams, _d) + 1;
    } else {
      genError(part);
      return -1;
    }
  }


  // N12
  private Boolean genArrSize(Vector<SSA> arrSize, List<Integer> arrshape, pack _d, Boolean enable_phi) {
    final String part = "NT_arr_size";
    // "["
    if(!nextSymEqualTo(Token.openbracketToken, part)) return null;
    // N18
    arrshape.add(genRvalue(arrSize, _d, enable_phi));
    // "]"
    if(!nextSymEqualTo(Token.closebracketToken, part)) return null;
    // N27
    return genOptArrSize(arrSize, arrshape, _d, enable_phi);
  }

  // N27
  private Boolean genOptArrSize(Vector<SSA> optArrSize, List<Integer> arrshape,pack _d, Boolean enable_phi) {
    final String part = "NT_opt_arr_size";
    nextSym();
    if(genSym == Token.openbracketToken) {
      resetSym();
      // N18
      arrshape.add(genRvalue(optArrSize, _d, enable_phi));
      // "]"
      if(!nextSymEqualTo(Token.closebracketToken, part)) return false;
      // N27
      genOptArrSize(optArrSize, arrshape, _d, enable_phi);
      return true;
    } else if (genSym == Token.identToken || genSym == Token.timesToken || genSym == Token.divToken ||
            genSym == Token.plusToken || genSym == Token.minusToken || genSym == Token.eqlToken ||
            genSym == Token.neqToken || genSym == Token.lssToken || genSym == Token.geqToken ||
            genSym == Token.leqToken || genSym == Token.gtrToken || genSym == Token.commaToken ||
            genSym == Token.closebracketToken || genSym == Token.closeparenToken || genSym == Token.becomesToken ||
            genSym == Token.thenToken || genSym == Token.doToken || genSym == Token.endToken ||
            genSym == Token.semiToken || genSym == Token.odToken || genSym == Token.fiToken ||
            genSym == Token.elseToken) {
      return true;
    }
    genError(part);
    return false;
  }

  // N10
  private Boolean genBlock(Vector<SSA> block, pack _d) {
    final String part = "NT_block";
    nextSym();
    if(genSym == Token.letToken || genSym == Token.callToken || genSym == Token.ifToken ||
            genSym == Token.whileToken || genSym == Token.returnToken) {
      // N13
      genSentence(block, _d);
      // N29
      genOptBlock(block, _d);
      return true;
    } else {
      genError(part);
      return false;
    }
  }

  // N11
  private Boolean genReturnStatement(Vector<SSA> _return, pack _d) {
    final String part = "return";
    if(!nextSymEqualTo(Token.returnToken, part)) return false;
    // N18
    Integer ret = genRvalue(_return, _d, false);
    _return.add(new SSA(_d.PC++, "ret", ret, -1));
    return true;
  }

  // N13
  private Boolean genSentence(Vector<SSA> sentence,pack _d){
    final String part = "NT_sentence";
    nextSym();
    if(genSym == Token.letToken) {
      return genAssignment(sentence, _d);
    } else if(genSym == Token.callToken) {
      return genCall(sentence, _d, false);
    } else if(genSym == Token.ifToken) {
      return genBranch(sentence, _d);
    } else if(genSym == Token.whileToken) {
      return genWhileloop(sentence, _d);
    } else if(genSym == Token.returnToken) {
      return genReturnStatement(sentence, _d);
    } else {
      genError(part);
      return false;
    }
  }

  // N14
  private Boolean genAssignment(Vector<SSA> assignment, pack _d) {
    final String part = "NT_assignment";
    // "let"
    if(!nextSymEqualTo(Token.letToken, part)) return false;
    // N34
    Pair<Integer, Integer> pos_data_p = genLvalue(assignment, true, _d, false);
    if(pos_data_p.getKey() == -1) { genError(part); return false;} // data not initialized.
    // "<-"
    if(!nextSymEqualTo(Token.becomesToken, part)) return false;
    // N18
    Integer data_p = genRvalue(assignment, _d, false);
    if(data_p == -1) {genError(part); System.out.println("gen rvalue res, data_p == -1;"); return false;}
    _d.data.get(data_p).uses.add(_d.PC);
    if(pos_data_p.getValue() == -1) { // var
      _d.varpos.set(pos_data_p.getKey(), data_p);
    } else if(_d.data.get(_d.varpos.get(pos_data_p.getKey())).type == varnode.Array) {
      assignment.add(new SSA(_d.PC++, "store", data_p, pos_data_p.getValue()));
    }
    return true;
  }

  // N15
  private Boolean genCall(Vector<SSA> _call, pack _d, Boolean enable_phi) {
    final String part = "_call";
    // "call"
    if(!nextSymEqualTo(Token.callToken, part)) return false;
    // "id"
    if(!nextSymEqualTo(Token.identToken, part)) return false;
    Integer funcid = sr.id;
    // N19
    List<Integer> params = genOptActualParams(_call, _d, enable_phi);
    // call
    for (Integer param : params) {
      _call.add(new SSA(_d.PC++, "param", param, -1));
    }
    varnode node = new varnode(_d.PC, varnode.Expression);
    _d.data.add(node);
    _call.add(new SSA(_d.PC++, "call", globalvarpos.get(globalid.get(funcid).pos), -1));
    return true;
  }

  // N16
  private Boolean genBranch(Vector<SSA> branch, pack _d) {
    final String part = "NT_branch";
    // "if"
    if(!nextSymEqualTo(Token.ifToken, part)) return false;
    // N20
    Integer condpos = genCondition(branch, _d, false);
    // "then"
    if(!nextSymEqualTo(Token.thenToken, part)) return false;
    // N10
    List<Integer> lvarpos = new Vector<>(_d.varpos);
    pack _ld = new pack(); _ld.idmap = _d.idmap;
    _ld.varpos = lvarpos; _ld.PC = _d.PC; _ld.dict = _d.dict;
    _ld.data = _d.data;
    genBlock(branch, _ld);
    _d.PC = _ld.PC;
    // N21
    List<Integer> rvarpos = new Vector<>(_d.varpos);
    pack _rd = new pack(); _rd.idmap = _d.idmap;
    _rd.varpos = rvarpos; _rd.PC = _d.PC;
    _rd.data = _d.data; _rd.dict = _d.dict;
    Integer brapos = genOptElseBranch(branch, _rd);
    _d.PC = _rd.PC;
    // "fi"
    if(!nextSymEqualTo(Token.fiToken, part)) return false;
    // set condition SSA and bra SSA
    branch.get(condpos).y = dataGetConst(brapos+1, _d);
    branch.get(brapos).y = dataGetConst(_d.PC, _d);
    for(int i = 0; i < _d.varpos.size(); i++) {
      if((!lvarpos.get(i).equals(_d.varpos.get(i)) || !rvarpos.get(i).equals(_d.varpos.get(i))) &&
              (_d.varpos.get(i) == -1 || _d.data.get(_d.varpos.get(i)).type == varnode.Var)) {
        if(lvarpos.get(i).equals(rvarpos.get(i))) {
          _d.varpos.set(i, lvarpos.get(i));
        } else {
          List<Integer> phi = new Vector<>();
          phi.add(lvarpos.get(i)); phi.add(rvarpos.get(i));
          dataGetExpression(branch, "phi", phi, _d);
        }
      }
    }
    return true;
  }

  // N17
  private Boolean genWhileloop(Vector<SSA> whileloop, pack _d) {
    final String part = "NT_whileloop";
    // "while"
    if(!nextSymEqualTo(Token.whileToken, part)) return false;
    // N20
    Vector<SSA> condition = new Vector<>();
    pack _cd = new pack();
    _cd.PC = 0; _cd.idmap = _d.idmap;
    _cd.data = _d.data; _cd.varpos = new Vector<>(_d.varpos);
    _cd.dict = _d.dict;
    Integer condpos = genCondition(condition, _cd, true);
    // "do"
    if(!nextSymEqualTo(Token.doToken, part)) return false;
    // N10
    Vector<SSA> block = new Vector<>();
    pack _bd = new pack(); _bd.idmap = _cd.idmap;
    _bd.varpos = new Vector<>(_cd.varpos); _bd.PC = 0;
    _bd.dict = _cd.dict; _bd.data = _cd.data;
    genBlock(block, _bd);
    // "od"
    if(!nextSymEqualTo(Token.odToken, part)) return false;
    // post process
    // get changed value in the block
    List<Integer> difset = new Vector<>();
    for(Integer id : _d.idmap.keySet()) {
      if(_bd.varpos.get(_bd.idmap.get(id).pos) != _cd.varpos.get(_cd.idmap.get(id).pos)) {
        difset.add(id);
      }
    }
    for (SSA ssa : condition) {
      if(ssa.op.equals("uphi")) {
        if(difset.contains(ssa.xs.get(0))) {
          List<Integer> phi = new Vector<>();
          Integer varid = ssa.xs.get(0);
          phi.add(_d.varpos.get(_d.idmap.get(varid).pos));
          phi.add(_bd.varpos.get(_bd.idmap.get(varid).pos));
          Integer data_p = _cd.varpos.get(_cd.idmap.get(varid).pos);
          if (_cd.data.get(data_p).type != varnode.Alias) {
            genError(part);
            return false;
          }
          _cd.data.get(data_p).type = varnode.Expression;
          _cd.data.get(data_p).def = _d.PC;
          _d.data.add(_cd.data.get(data_p));
          data_p = _d.data.size()-1;
          String cse_key = "phi";
          for (Integer x : phi) {
            cse_key += x.toString();
            cse_key += ",";
          }
          if (_d.dict.containsKey(cse_key)) {
            genError(part);
            return false;
          }
          _d.dict.put(cse_key, data_p);
          _d.varpos.set(_d.idmap.get(varid).pos, data_p);
          whileloop.add(new SSA(_d.PC++, "phi", phi));
        } else {
          condpos --;
        }
      }
    }

    for (SSA ssa : condition) {
      if(!ssa.op.equals("uphi")) {
        Boolean hasalias = false;
        if(!(ssa.op.equals("beq") || ssa.op.equals("bne") || ssa.op.equals("ble") || ssa.op.equals("blt") ||
        ssa.op.equals("bge") || ssa.op.equals("bgt") || ssa.op.equals("end") || ssa.op.equals("store"))) {
          Integer prev_data_p = dataGetExpression(whileloop, ssa.op, ssa.x, ssa.y, _cd);
          if (ssa.x != -1 && _cd.data.get(ssa.x).type == varnode.Alias) {
            ssa.x = _cd.data.get(ssa.x).val;
            hasalias = true;
          }
          if (ssa.y != -1 && _cd.data.get(ssa.x).type == varnode.Alias) {
            ssa.y = _cd.data.get(ssa.y).val;
            hasalias = true;
          }
          if (hasalias) {
            Integer data_p = dataGetExpression(whileloop, ssa.op, ssa.x, ssa.y, _d);
            if (prev_data_p != data_p) {
              _d.data.get(prev_data_p).type = varnode.Alias;
              _d.data.get(prev_data_p).val = data_p;
            } else {
              _d.data.get(prev_data_p).def = _d.PC - 1;
            }
          } else {
            whileloop.add(ssa);
            _d.PC ++;
          }
        } else {
          whileloop.add(ssa);
          _d.PC ++;
        }
      }
    }
    whileloop.get(_d.PC-1).y = dataGetConst(_d.PC + block.size(), _d);

    for (SSA ssa : block) {
      Boolean hasalias = false;
      if(!(ssa.op.equals("beq") || ssa.op.equals("bne") || ssa.op.equals("ble") || ssa.op.equals("blt") ||
              ssa.op.equals("bge") || ssa.op.equals("bgt") || ssa.op.equals("end") || ssa.op.equals("store"))) {
        Integer prev_data_p = dataGetExpression(whileloop, ssa.op, ssa.x, ssa.y, _bd);
        if (ssa.x != -1 && _bd.data.get(ssa.x).type == varnode.Alias) {
          ssa.x = _bd.data.get(ssa.x).val;
          hasalias = true;
        }
        if (ssa.y != -1 && _bd.data.get(ssa.x).type == varnode.Alias) {
          ssa.y = _bd.data.get(ssa.y).val;
          hasalias = true;
        }
        if (hasalias) {
          Integer data_p = dataGetExpression(whileloop, ssa.op, ssa.x, ssa.y, _d);
          if (prev_data_p != data_p) {
            _d.data.get(prev_data_p).type = varnode.Alias;
            _d.data.get(prev_data_p).val = data_p;
          } else {
            _d.data.get(prev_data_p).def = _d.PC - 1;
          }
        } else {
          whileloop.add(ssa);
          _d.PC ++;
        }
      } else {
        whileloop.add(ssa);
        _d.PC ++;
      }
    }
    return true;
  }

  // N18 return data_p
  private Integer genRvalue(Vector<SSA> rvalue, pack _d, Boolean enable_phi) {
    final String part = "NT_rvalue";
    nextSym();
    if(genSym == Token.numberToken || genSym == Token.identToken || genSym == Token.openparenToken
      || genSym == Token.callToken) {
      return genExpression(rvalue, _d, enable_phi);
    } else {
      genError(part);
      return -1;
    }
  }

  // N19
  private List<Integer> genOptActualParams(Vector<SSA> optactualparams, pack _d, Boolean enable_phi) {
    final String part = "optactualparams";
    List<Integer> params = new Vector<>();
    nextSym();
    if(genSym == Token.semiToken || genSym == Token.endToken || genSym == Token.odToken ||
            genSym == Token.fiToken || genSym == Token.elseToken) {
      return params;
    } else if(genSym == Token.openparenToken) {
      resetSym();
      // N3
      params.add(genOptRvalue(optactualparams, _d, enable_phi));
      // N23
      params.addAll(genOptContActualParams(optactualparams, _d, enable_phi));
      // ")"
      if(!nextSymEqualTo(Token.closeparenToken, part)) return null;
      return params;
    } else {
      genError(part);
      return null;
    }
  }

  // N3 return data_p
  private Integer genOptRvalue(Vector<SSA> optrvalue, pack _d, Boolean enable_phi) {
    final String part = "optrvalue";
    nextSym();
    if(genSym == Token.numberToken || genSym == Token.identToken || genSym == Token.callToken ||
            genSym == Token.openparenToken) {
      return genRvalue(optrvalue, _d, enable_phi);
    } else if (genSym == Token.closeparenToken) {
      return -1;
    } else {
      genError(part);
      return -1;
    }
  }

  // N20, return ssapos of branch operation
  private Integer genCondition(Vector<SSA> condition, pack _d, Boolean enable_phi) {
    final String part = "NT_condition";
    nextSym();
    if(genSym == Token.identToken || genSym == Token.numberToken || genSym == Token.openparenToken ||
            genSym == Token.callToken) {
      // N18
      Integer l = genRvalue(condition, _d, enable_phi);
      // N30
      nextSym();
      Integer relop = genSym;
      resetSym();
      // N18
      Integer r = genRvalue(condition, _d, enable_phi);
      // SSA
      Integer cmp = dataAddExpression("cmp", l, r, _d);
      condition.add(new SSA(_d.PC++, "cmp", l, r));
      switch (relop) {
        case Token.eqlToken : {
          condition.add(new SSA(_d.PC++, "bne", cmp, -1));
          return(_d.PC-1);
        }
        case Token.lssToken : {
          condition.add(new SSA(_d.PC++, "bge", cmp, -1));
          return(_d.PC-1);
        }
        case Token.leqToken : {
          condition.add(new SSA(_d.PC++, "bgt", cmp, -1));
          return(_d.PC-1);
        }
        case Token.geqToken : {
          condition.add(new SSA(_d.PC++, "blt", cmp, -1));
          return(_d.PC-1);
        }
        case Token.gtrToken : {
          condition.add(new SSA(_d.PC++, "ble", cmp, -1));
          return(_d.PC-1);
        }
        case Token.neqToken : {
          condition.add(new SSA(_d.PC++, "beg", cmp, -1));
          return(_d.PC-1);
        }
        default: {
          genError(part);
          return -1;
        }
      }
    } else {
      genError(part);
      return -1;
    }
  }

  // N21, return the first line of this block(which is the bra of the former if any). return 0 if no else branch.
  private Integer genOptElseBranch(Vector<SSA> optelsebranch, pack _d) {
    final String part = "NT_optelsebranch";
    nextSym();
    if(genSym == Token.fiToken || genSym == Token.returnToken) {
      return 0;
    } else if(genSym == Token.elseToken) {
      resetSym();
      Integer brapos = _d.PC;
      optelsebranch.add(new SSA(_d.PC++, "bra", -1, -1));
      // N10
      genBlock(optelsebranch, _d);
      return brapos;
    } else {
      genError(part);
      return -1;
    }
  }

  // N22
  private Integer genContFormalParams(Vector<SSA> cfp, pack _d) {
    final String part = "contformalparams";
    nextSym();
    if(genSym == Token.closeparenToken) {
      return 0;
    } else if(genSym == Token.commaToken) {
      resetSym();
      // "id"
      if(!nextSymEqualTo(Token.identToken, part)) return -1;
      varinfo info = new varinfo();
      _d.data.add(new varnode(-2, varnode.FormalParam));
      info.pos = _d.data.size()-1;
      _d.idmap.put(sr.id, info);
      // N22
      return genContFormalParams(cfp, _d) + 1;
    } else {
      genError(part);
      return -1;
    }
  }

  // N23
  private List<Integer> genOptContActualParams(Vector<SSA> ocap, pack _d, Boolean enable_phi) {
    final String part = "genoptcontacualparams";
    List<Integer> params = new Vector<>();
    nextSym();
    if(genSym == Token.closeparenToken) {
      return params;
    } else if(genSym == Token.commaToken) {
      resetSym();
      // N18
      params.add(genRvalue(ocap, _d, enable_phi));
      // N23
      params.addAll(genOptContActualParams(ocap, _d, enable_phi));
      return params;
    } else {
      genError(part);
      return null;
    }
  }

  // N24 to be modified
  private Integer genFactor(Vector<SSA> factor, pack _d, Boolean enable_phi) {
    final String part = "NT_factor";
    nextSym();
    if(genSym == Token.identToken) {
      resetSym();
      Integer id = sr.id;
      varinfo var = _d.idmap.get(sr.id);
      Integer pos = var.pos;
      if(pos == -1) return -1;
      Vector<Integer> optarrpos = new Vector<>();
      genOptArrSize(factor, optarrpos, _d, enable_phi);
      if(optarrpos.size() == 0) {
        if (enable_phi) {
          Integer prev_data_p = _d.varpos.get(_d.idmap.get(id).pos);
          if(_d.data.get(prev_data_p).type == varnode.Alias) {
            return prev_data_p;
          }
          List<Integer> phi = new Vector<>(); phi.add(id);
          Integer data_p = dataGetExpression(factor, "uphi", phi, _d); // abnormal form!!!!!!!
          _d.data.get(data_p).type = varnode.Alias;
          _d.data.get(data_p).val = prev_data_p;
          _d.varpos.set(_d.idmap.get(id).pos, data_p);
          return data_p;
        } else {
          return _d.varpos.get(pos);
        }
      }
      else {
        Integer elempos = getArrElem(factor, var, optarrpos, _d, enable_phi);
        varnode node = new varnode(_d.PC, varnode.Expression);
        _d.data.add(node);
        factor.add(new SSA(_d.PC++, "load", -1, elempos));
        return _d.data.size()-1;
      }
    } else if(genSym == Token.numberToken) {
      resetSym();
      return dataGetConst(sr.number, _d);
    } else if(genSym == Token.openparenToken) {
      resetSym();
      // N26
      Integer data_p = genExpression(factor, _d, enable_phi);
      // ")"
      if(!nextSymEqualTo(Token.closeparenToken, part)) return -1;
      return data_p;
    } else if(genSym == Token.callToken) {
      return -1;
    } else {
      genError(part);
      return -1;
    }
  }

  // N25 && N31
  private Integer genTerm(Vector<SSA> term, pack _d, Boolean enable_phi) {
    final String part = "NT_term";
    nextSym();
    if(genSym == Token.identToken || genSym == Token.numberToken || genSym == Token.openparenToken ||
        genSym == Token.callToken) {
      // N24
      Integer factor_data_p = genFactor(term, _d, enable_phi);
      // N31
      nextSym();
      while (genSym == Token.timesToken || genSym == Token.divToken) {
        boolean times = genSym == Token.timesToken;
        resetSym();
        Integer n_factor_data_p = genFactor(term, _d, enable_phi);
        if(times) {
          // if can be calculated directly
          if(_d.data.get(factor_data_p).type == varnode.Const && _d.data.get(n_factor_data_p).type == varnode.Const) {
            Integer val = _d.data.get(factor_data_p).val * _d.data.get(n_factor_data_p).val;
            factor_data_p = dataGetConst(val, _d);
          } else { // can't be calculated directly
            factor_data_p = dataGetExpression(term, "mul", factor_data_p, n_factor_data_p, _d);
          }
        } else {
          // if can be calculated directly
          if(_d.data.get(factor_data_p).type == varnode.Const && _d.data.get(n_factor_data_p).type == varnode.Const) {
            Integer val = _d.data.get(factor_data_p).val / _d.data.get(n_factor_data_p).val;
            factor_data_p = dataGetConst(val, _d);
          } else { // can't be calculated directly
            factor_data_p = dataGetExpression(term, "div", factor_data_p, n_factor_data_p, _d);
          }
        }
        nextSym();
      }
      return factor_data_p;
    } else {
      genError(part);
      return -1;
    }
  }

  // N26 && N32
  private Integer genExpression(Vector<SSA> expression, pack _d, Boolean enable_phi) {
    final String part = "NT_expression";
    nextSym();
    if(genSym == Token.identToken || genSym == Token.numberToken || genSym == Token.openparenToken ||
        genSym == Token.callToken) {
      // N25
      Integer term_data_p = genTerm(expression, _d, enable_phi);
      // N32
      nextSym();
      while (genSym == Token.plusToken || genSym == Token.minusToken) {
        boolean plus = genSym == Token.plusToken;
        resetSym();
        Integer n_term_data_p = genTerm(expression, _d, enable_phi);
        if(plus) {
          // if can be calculated directly
          if(_d.data.get(term_data_p).type == varnode.Const && _d.data.get(n_term_data_p).type == varnode.Const) {
            Integer val = _d.data.get(term_data_p).val + _d.data.get(n_term_data_p).val;
            term_data_p = dataGetConst(val, _d);
          } else { // can't be calculated directly
            term_data_p = dataGetExpression(expression, "add", term_data_p, n_term_data_p, _d);
          }
        }
        else {
          // if can be calculated directly
          if(_d.data.get(term_data_p).type == varnode.Const && _d.data.get(n_term_data_p).type == varnode.Const) {
            Integer val = _d.data.get(term_data_p).val - _d.data.get(n_term_data_p).val;
            term_data_p = dataGetConst(val, _d);
          } else { // can't be calculated directly
            term_data_p = dataGetExpression(expression, "sub", term_data_p, n_term_data_p, _d);
          }
        }
        nextSym();
      }
      return term_data_p;
    } else {
      genError(part);
      return -1;
    }
  }

  // N28
  private Vector<Integer> genOptContId() {
    final String part = "NT_optContId";
    Vector<Integer> optContId = new Vector<>();
    nextSym();
    if(genSym == Token.commaToken) {
      resetSym();
      if(!nextSymEqualTo(Token.identToken, part)) return null;
      optContId.add(sr.id);
      optContId.addAll(genOptContId());
      return optContId;
    } else if(genSym == Token.semiToken) {
      return optContId;
    } else {
      genError(part);
      return null;
    }
  }

  // N29
  private Boolean genOptBlock(Vector<SSA> optBlock, pack _d) {
    final String part = "NT_optblock";
    nextSym();
    if(genSym == Token.semiToken) {
      resetSym();
      // N33
      return genOptContBlock(optBlock, _d);
    } else if(genSym == Token.endToken || genSym == Token.odToken || genSym == Token.fiToken ||
        genSym == Token.elseToken) {
      return true;
    } else {
      genError(part);
      return false;
    }
  }

  // N33
  private Boolean genOptContBlock(Vector<SSA> optContBlock, pack _d) {
    final String part = "NT_optContBlock";
    nextSym();
    if(genSym == Token.letToken || genSym == Token.callToken || genSym == Token.ifToken ||
        genSym == Token.whileToken || genSym == Token.returnToken) {
      return genBlock(optContBlock, _d);
    } else {
      genError(part);
      return false;
    }
  }

  // N34, for int, return pos; for array, return pos for the specific elem, i.e. data_p
  private Pair<Integer, Integer> genLvalue(Vector<SSA> lvalue, Boolean isassignment, pack _d, Boolean enable_phi){
    final String part = "NT_lvalue";
    nextSym();
    if(!nextSymEqualTo(Token.identToken, part)) return new Pair<>(-1, -1);
    if(!_d.idmap.containsKey(sr.id)) return new Pair<>(-1, -1);
    varinfo var = _d.idmap.get(sr.id);
    // optional "["
    if(!var.isarray) {
      return new Pair<>(var.pos, -1);
    } else {
      // insert to data and varpos
      if(isassignment && _d.varpos.get(var.pos) == -2) {
        _d.varpos.set(var.pos, dataAddArray(sr.id, _d));
      }
      List<Integer> arrpos = new Vector<>();
      genArrSize(lvalue, arrpos, _d, enable_phi);
      return new Pair<>(var.pos, getArrElem(lvalue, var, arrpos, _d, enable_phi));
    }
  }

  // N35
  private Integer genProcOptFormalParams(Vector<SSA> pofp, pack _d) {
    final String part = "procoptformalparams";
    nextSym();
    if(genSym == Token.semiToken) {
      return 0;
    } else if (genSym == Token.openparenToken) {
      resetSym();
      // N8
      Integer num = genOptFormalParams(pofp, _d);
      // ")"
      if (!nextSymEqualTo(Token.closeparenToken, part)) return -1;
      return num;
    } else {
      genError(part);
      return -1;
    }
  }

  private boolean nextSymEqualTo(int token, final String part) {
    nextSym();
    if(genSym != token) {
      genError(part);
      return false;
    }
    resetSym();
    return true;
  } // not go into NT

  private void nextSym() {
    if(genSym == -1) {
      genSym = sr.GetSym();
      if(genSym != 60 && genSym != 61) {
        System.out.print(Utils.decons[genSym]);
      } else if(genSym == 60){
        System.out.print(sr.number);
      } else {
        System.out.print("var");
        System.out.print(sr.id);
      }
    }
  }

  private void resetSym() {
    genSym = -1;
  }

  private void genError(final String part) {
    errMsg = "Generate Error! In the part: " + part;
    resetSym();
  }

  private Integer dataGetConst(Integer val, pack _d) {
    String cse_key = "const@" + val.toString();
    if(!_d.dict.containsKey(cse_key)) {
      return dataAddConst(val, _d);
    } else {
      return _d.dict.get(cse_key);
    }
  }

  private Integer dataGetExpression(Vector<SSA> dge, String op, Integer data_p_x, Integer data_p_y, pack _d) {
    String cse_key = op + data_p_x.toString() + ',' + data_p_y.toString();
    if (!_d.dict.containsKey(cse_key)) {
      Integer tmp = dataAddExpression(op, data_p_x, data_p_y, _d);
      _d.data.get(data_p_x).uses.add(_d.PC); _d.data.get(data_p_y).uses.add(_d.PC);
      dge.add(new SSA(_d.PC++, op, data_p_x, data_p_y));
      return tmp;
    } else {
      return _d.dict.get(cse_key);
    }
  }
  private Integer dataGetExpression(Vector<SSA> dge, String _phi, List<Integer> _xs, pack _d) {
    String cse_key = _phi;
    for (Integer x : _xs) { cse_key += x.toString(); cse_key += ","; }
    if (!_d.dict.containsKey(cse_key)) {
      Integer tmp = dataAddExpression(_phi, _xs, _d);
      for (Integer x : _xs)  if(!x.equals(-1)) _d.data.get(x).uses.add(_d.PC);
      dge.add(new SSA(_d.PC++, _phi, _xs));
      return tmp;
    } else {
      return _d.dict.get(cse_key);
    }
  }
  // always earlier than insert to SSA vector
  private Integer dataAddConst(Integer val, pack _d) {
    varnode node = new varnode(_d.PC, varnode.Const);
    node.val = val; _d.data.add(node);
    _d.dict.put("const@"+val.toString(), _d.data.size()-1);
    return _d.data.size()-1;
  }

  // 遇到array直接跳
  private Integer dataAddExpression(String op, Integer x, Integer y, pack _d) {
    // x or y is not const
    varnode node = new varnode(_d.PC, varnode.Expression);
    _d.data.add(node); _d.dict.put(op + x.toString() + ',' + y.toString(), _d.data.size()-1);
    return _d.data.size()-1;
  }

  private Integer dataAddExpression(String _phi, List<Integer> _xs, pack _d) {
    varnode node = new varnode(_d.PC, varnode.Expression);
    _d.data.add(node); String str = _phi;
    for (Integer x : _xs) { str += x.toString(); str += ","; }
    _d.dict.put(str, _d.data.size()-1);
    return _d.data.size()-1;
  }

  private Integer dataUpdateExpression(Integer data_p, String _phi, List<Integer> _xs, pack _d) {
    String str = _phi;
    for (Integer x : _xs) { str += x.toString(); str += ","; }
    _d.dict.put(str, data_p);
    return data_p;
  }
  private Integer dataAddVar(Integer id, pack _d) {
    varnode node = new varnode(_d.PC, varnode.Var);
    _d.data.add(node); _d.dict.put("var@" + id.toString(), _d.data.size()-1);
    return _d.data.size()-1;
  }

  private Integer dataAddArray(Integer id, pack _d) {
    varnode node = new varnode(_d.PC, varnode.Array);
    _d.data.add(node); _d.dict.put("array@" + id.toString(), _d.data.size()-1);
    return _d.data.size()-1;
  }

  private Integer getArrElem(Vector<SSA> genarrelem, varinfo var, List<Integer> arrpos, pack _d, Boolean enable_phi) {
    final String part = "getarrelem";
    if (arrpos.size() != var.dim) { genError(part); return -1;}
    Integer add = 0;
    for (int i = 0; i < arrpos.size(); i++) {
      if (_d.data.get(arrpos.get(i)).type == varnode.Const) {
        Integer val = _d.data.get(arrpos.get(i)).val * var.arrbias.get(i);
        Integer _c = dataGetConst(val, _d);
        if(i == 0)
          add = _c;
        else {
          if(_d.data.get(add).type == varnode.Const) {
            add = dataGetConst(_d.data.get(add).val + _d.data.get(_c).val, _d);
          } else {
            add = dataGetExpression(genarrelem, "add", add, _c, _d);
          }
        }
      } else {
        Integer l = dataGetConst(var.arrbias.get(i), _d);
        Integer r = arrpos.get(i);
        Integer exp = dataGetExpression(genarrelem,"mul", l, r, _d);
        if(i == 0)
          add = exp;
        else
          add = dataGetExpression(genarrelem, "add", add, exp, _d);
      }
    }
    return dataGetExpression(genarrelem, "adda", _d.varpos.get(var.pos), add, _d);
  }

  private FuncProc InputNum() {
    FuncProc inputnum = new FuncProc();
    inputnum.paramnum = 0;
    inputnum.funcid = 0;
    inputnum.ssa.add(new SSA(0, "read", -1, -1));
    inputnum.data.add(new varnode(0, varnode.Var));
    inputnum.ssa.add(new SSA(1, "ret", 0, -1));
    globaldata.add(new varnode(-2, varnode.Function));
    globalvarpos.add(0);
    varinfo info = new varinfo(); info.pos = 0;
    globalid.put(0, info);
    return inputnum;
  }

  private FuncProc OutputNum() {
    FuncProc outputnum = new FuncProc();
    outputnum.paramnum = 1;
    outputnum.funcid = 1;
    outputnum.data.add(new varnode(-2, varnode.FormalParam));
    outputnum.ssa.add(new SSA(0, "write", 0, -1));
    globaldata.add(new varnode(-2, varnode.Function));
    globalvarpos.add(1);
    varinfo info = new varinfo(); info.pos = 1;
    globalid.put(1, info);
    return outputnum;
  }
}
