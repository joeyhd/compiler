public class Parser {

  /* moved up from Scanner */
  private int scannerSym; // the current token on the input
  private Scanner sr;
  private void Next() {
    scannerSym = sr.GetSym();
  }

  public Parser(String fileName) {
    sr = new Scanner(fileName);
    Next();
  }

  public void Error(String errorMst) {
    scannerSym = Token.errorToken;
    sr.Error(errorMst);
  }

  // temp exam
  private int expression() {
    int val = term();
    while (scannerSym == Token.plusToken) {
        Next();
        val += term();
    }
    return val;
  }

  private int term() {
    int val = factor();
    while(scannerSym == Token.timesToken) {
      Next();
      val *= factor();
    }
    return val;
  }

  private int factor() {
    int val = 0;
    if (scannerSym == Token.numberToken) {
      val = sr.number;
      Next();
    } else if (scannerSym == Token.openparenToken) {
      Next();
      val = expression();
      if(scannerSym == Token.closeparenToken) {
        Next();
      } else {
        Error("expression error");
      }
    }
    return val;
  }
}
