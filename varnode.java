import java.util.*;

public class varnode {
  Integer val; // if can be calculated it's a constant
  // Integer pred; // the pred node of current node, the position in list:data
  // List<Integer> succ; // the succ node of current node
  Integer type;
  Integer def ; // the define pos of current node
  List<Integer> uses; // the use pos of current node

  public varnode(Integer _def, Integer _type) {
    def = _def; uses = new Vector<>();
    type = _type;
  }

  static public int Const = 0;
  static public int Var = 1;
  static public int Array = 2;
  static public int Expression = 3;
  static public int FormalParam = 4;
  static public int Function = 5;
  static public int Procedure = 6;
  static public int Alias = 7;
  static public List<String> typestr = new Vector<>();
  {
    typestr.add("Const"); typestr.add("Var");
    typestr.add("Array"); typestr.add("Expression");
    typestr.add("FormalParam"); typestr.add("Function");
    typestr.add("Procedure"); typestr.add("Alias");
  }
}
