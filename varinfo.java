import java.util.*;

public class varinfo {
    Integer pos; // position in data. -1 means not initialized.
    Boolean isarray;
    List<Integer> arrshape; // e.g. [3, 4, 5]
    List<Integer> arrbias;
    Integer size; // 60
    Integer dim; // dimension // 3
    Integer address = -1; // bias address.

    public varinfo() {
        pos = -1; isarray = false; size = 1;
    }

    public varinfo(List<Integer> _arrshape) {
        pos = -1; isarray = true; dim = _arrshape.size(); arrbias = new Vector<>();
        arrshape = new Vector<>(_arrshape);
        size = 1; int bias = 1;
        for (int i = 0; i < _arrshape.size(); i++ ) {
            arrbias.add(bias);
            size *= _arrshape.get(i);
            bias *= _arrshape.get(_arrshape.size()-1-i);
        }
        for (int i = 0; i < arrbias.size()/2; i++) {
            Integer tmp = arrbias.get(i);
            arrbias.set(i, arrbias.get(arrbias.size()-1-i));
            arrbias.set(arrbias.size()-1-i, tmp);
        }
    }
}
