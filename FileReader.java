import java.io.BufferedReader;
import java.io.IOException;

public class FileReader{

  public char GetSym(){
    Next(); return sym;
  }

  public void Error(String errorMsg) { // signal an error with current file position
    sym = (char)0x00;
    System.out.println("Error at " + linenum + "," + pos + ": " + errorMsg);
  }

  public FileReader(String fileName) { // constructor: open file and read the first character into 'sym'
    try {
      br = new BufferedReader(new java.io.FileReader(fileName));
    } catch (IOException e) {
      Error(e.getMessage());
      return;
    }
  }

  private void Next() { // advance to the next character
    while(pos >= line.length()) { // read new line
      try {
        line = br.readLine();
        line = deleteComment(line);
      } catch (IOException e) {
        Error(e.getMessage());
        return;
      }
      linenum += 1; pos = -1;
      if (line == null) {
        sym = 0xff; // EOF
        return;
      }
    }
    if(pos == -1)
      sym = '\n';
    else
      sym = line.charAt(pos);
    pos++ ;
    return ;
  }

  private String deleteComment(String line) {
    if(line == null)
      return line;
    int _pos = line.indexOf('#');
    if (_pos >= 0 ) {
      line = line.substring(0, _pos);
    }
    _pos = line.indexOf("//");
    if (_pos >= 0 ) {
      line = line.substring(0, _pos);
    }
    return line;
  }
  private char sym; // the current character on the input, 0x00=error, 0xff=EOF
  private BufferedReader br;
  private String line = "";
  private int linenum = 0; // line number
  private int pos = -1;
}
